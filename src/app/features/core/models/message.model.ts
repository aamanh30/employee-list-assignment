export enum MessageType {
  success,
  failure
}

export interface Message {
  type: MessageType;
  content: string;
}
