import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnimationService {
  showLoader$: BehaviorSubject<boolean>;
  constructor() {
    this.showLoader$ = new BehaviorSubject(true);
  }

  setLoader(flag: boolean): void {
    this.showLoader$.next(flag);
  }
}
