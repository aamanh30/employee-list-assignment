import { EmployeesPageComponent } from './containers/employees-page/employees-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ':id',
        component: EmployeesPageComponent
      },
      {
        path: '',
        pathMatch: 'full',
        component: EmployeesPageComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'error/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
