import { EmployeeDetailsComponent } from './../../components/employee-details/employee-details.component';
import { Employee } from './../../models/employee';
import { EmployeesService } from './../../services/employees.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MessageType, Message } from '../../../core/models/message.model';

@Component({
  selector: 'app-employees-page',
  templateUrl: './employees-page.component.html',
  styleUrls: ['./employees-page.component.scss']
})
export class EmployeesPageComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  employees: Employee[] = [];
  employee: Employee;
  isEditable: boolean = false;
  message: Message;
  messageType = MessageType;

  @ViewChild('employeeDetails') employeeDetailsComponent: EmployeeDetailsComponent;

  constructor(
    private employeesService: EmployeesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.loadEmployees();
    this.subscribeToParams();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  loadEmployees(): void {
    const subscription = this.employeesService.fetchEmployees().subscribe(
      (res => {
        this.employees = res;
        this.resetMessage();
      }),
      (err => {
        this.employees = [];
        this.message = {
          type: MessageType.failure,
          content: `Something went wrong`
        };
        this.resetMessage();
      })
    );
    this.subscriptions.push(subscription);
  }

  subscribeToParams(): void {
    const subscription = this.route.params.subscribe(param => {
      this.employee = null;
      this.isEditable = false;
      const { id } = param;
      const employee = this.employees.find(employee => employee.id == id);
      if (!id || !employee) {
        return;
      }
      this.employeesService.setLoader(true);
      this.employee = employee;
    });
    this.subscriptions.push(subscription);
  }

  updateEmployee() {
    this.employeeDetailsComponent.submitted = true;
    if(this.employeeDetailsComponent.employeeForm.invalid) {
      return;
    }
    const { value } = this.employeeDetailsComponent.employeeForm;
    const subscription = this.employeesService.updateEmployee(value).subscribe(
      (res: Employee) => {
        this.employee = res;
        this.message = {
          type: MessageType.success,
          content: `Employee details have been updated successfully`
        };
        this.resetMessage();
      },
      (err) => {
        alert(err);
        this.message = {
          type: MessageType.failure,
          content: `Something went wrong`
        };
        this.resetMessage();
      }
    );
    this.subscriptions.push(subscription);
  }

  resetMessage() {
    setTimeout(() => this.message = null, 2000);
  }
}
