import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeesPageComponent } from './containers/employees-page/employees-page.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NewPhoneFormComponent } from './components/new-phone-form/new-phone-form.component';

@NgModule({
  declarations: [EmployeesPageComponent, EmployeeListComponent, EmployeeDetailsComponent, NewPhoneFormComponent],
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class EmployeesModule { }
