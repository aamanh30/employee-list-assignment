import { EmployeesService } from './../../services/employees.service';
import { AfterViewChecked, ChangeDetectionStrategy, Component, Input, OnChanges, ChangeDetectorRef } from '@angular/core';
import { Employee } from './../../models/employee';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeDetailsComponent implements OnChanges, AfterViewChecked {
  @Input() employee: Employee;
  @Input() isEditable: boolean = false;
  submitted: boolean = false;
  employeeForm: FormGroup;
  showLoader$: Observable<boolean> = this.employeesService.showLoader$;
  constructor(
    private formBuilder: FormBuilder,
    private employeesService: EmployeesService,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnChanges() {
    this.initForm();
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      this.employeesService.setLoader(false);
      this.cdRef.detectChanges();
    }, 2000);
  }

  initForm() {
    if (!this.employee) {
      return;
    }
    const { name, id, address, department, phones } = this.employee;
    this.employeeForm = this.formBuilder.group({
      name: [name, [Validators.required, Validators.minLength(2)]],
      id: [id, [Validators.required]],
      address: [address, [Validators.required, Validators.minLength(10)]],
      department: [department, [Validators.required, Validators.minLength(2)]],
      phones: this.getPhones(phones)
    });
  }

  getPhones(phones: string[]): FormArray {
    return this.formBuilder.array([...phones]);
  }

  addNewPhone(phone): void {
    const phones = (this.employeeForm.get('phones') as FormArray).value;
    phones.push(phone);
    this.employeeForm.markAsDirty();
  }
}
