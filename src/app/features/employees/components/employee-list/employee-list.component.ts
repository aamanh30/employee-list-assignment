import { Router } from '@angular/router';
import { EmployeesService } from './../../services/employees.service';
import { Employee } from './../../models/employee';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent {
  @Input() id = null;
  @Input() employees: Employee[] = [];
  constructor(
    private router: Router,
    public employeesService: EmployeesService
  ) { }

  navigate(id): void {
    this.employeesService.setLoader(true);
    this.router.navigate([`/employees/${id}`]);
  }
}
