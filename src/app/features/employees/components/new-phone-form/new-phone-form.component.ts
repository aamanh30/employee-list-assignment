import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-new-phone-form',
  templateUrl: './new-phone-form.component.html',
  styleUrls: ['./new-phone-form.component.scss']
})
export class NewPhoneFormComponent implements OnInit {
  @Output() newPhoneEmitter: EventEmitter<any> = new EventEmitter();
  submitted: boolean = false;
  newPhoneForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.newPhoneForm = this.formBuilder.group({
      phone: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
    });
  }

  addNewPhone() {
    this.submitted = true;
    if(this.newPhoneForm.invalid) {
      return;
    }
    const { phone } = this.newPhoneForm.value;
    this.newPhoneEmitter.emit(phone);
    this.submitted = false;
    this.newPhoneForm.reset();
  }
}
