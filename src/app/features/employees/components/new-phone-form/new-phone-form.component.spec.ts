import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPhoneFormComponent } from './new-phone-form.component';

describe('NewPhoneFormComponent', () => {
  let component: NewPhoneFormComponent;
  let fixture: ComponentFixture<NewPhoneFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPhoneFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPhoneFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
