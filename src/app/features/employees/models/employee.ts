export interface Employee {
  name: string;
  id: number;
  address: string;
  department: string;
  phones: string[];
}
