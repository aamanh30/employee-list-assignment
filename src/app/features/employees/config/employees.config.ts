import { Employee } from './../models/employee';
export const employees: Employee[] = [
  {
    name: `John Smith`,
    id: 123,
    address: `123 Main Street, New York, NY 10030`,
    department: `Finance`,
    phones: [`9283726293`]
  },
  {
    name: `Peter Tyson`,
    id: 124,
    address: `124 Main Street, New York, NY 10024`,
    department: `Marketing`,
    phones: [`9283746203`, `9382716394`]
  },
  {
    name: `Mary`,
    id: 125,
    address: `125 Main Street, New York, NY 10025`,
    department: `IT`,
    phones: [`9482736103`, `9183746284`]
  },
  {
    name: `Jennifer`,
    id: 126,
    address: `126 Main Street, New York, NY 10026`,
    department: `Finance`,
    phones: []
  }
];
