import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Employee } from './../models/employee';
import { employees } from './../config/employees.config';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  employees$: BehaviorSubject<Employee[]>;
  showLoader$: BehaviorSubject<boolean>;
  constructor() {
    this.employees$ = new BehaviorSubject(employees);
    this.showLoader$ = new BehaviorSubject(false);
  }

  fetchEmployees(): Observable<Employee[]> {
    return this.employees$;
  }

  setEmployees(employees: Employee[]): void {
    this.employees$.next([...employees]);
  }

  updateEmployee(data: Employee) {
    const employees = this.employees$.getValue();
    const idx = employees.findIndex(employee => employee.id === data.id);
    employees[idx] = data;
    this.setEmployees([...employees]);
    return of(employees[idx]);
  }

  setLoader(flag: boolean): void {
    this.showLoader$.next(flag);
  }
}
