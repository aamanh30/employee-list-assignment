import { AnimationService } from './features/shared/services/animation.service';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  showLoader$: Observable<boolean>;

  constructor(
    private animationService: AnimationService
  ) {}

  ngOnInit() {
    this.showLoader$ = this.animationService.showLoader$;
    this.animationService.setLoader(true);
  }

  ngAfterViewInit() {
    setTimeout(() => this.animationService.setLoader(false), 3000);
  }
}
